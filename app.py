from flask import Flask, render_template, url_for, redirect, request, flash
from flask_mysqldb import MySQL

app = Flask(__name__)
app.config["MYSQL_HOST"] = "localhost"
app.config["MYSQL_USER"] = "root"
app.config["MYSQL_PASSWORD"] = ""
app.config["MYSQL_DB"] = "employee"
app.config["MYSQL_CURSORCLASS"] = "DictCursor"
db = MySQL(app)



@app.route("/")
def Index():
    con = db.connection.cursor()
    sql = "SELECT * FROM employee_table"
    con.execute(sql)
    result = con.fetchall()
    return render_template("index.html", datas=result)
@app.route("/")
def home():
    con = db.connection.cursor()
    sql = "SELECT * FROM salary"
    con.execute(sql)
    res = con.fetchall()
    return render_template("salaryUI.html",dataa=res)

@app.route("/")
def salaryui():
    con = db.connection.cursor()
    sql = "SELECT * FROM salary"
    con.execute(sql)
    res = con.fetchall()
    return render_template("salaryUI.html",dataa=res)
@app.route("/add", methods=['GET', 'POST'])
def add():

    con = db.connection.cursor()

    if request.method == 'POST':
        name = request.form['name']
        address = request.form['address']
        gender = request.form['gender']
        joineddate= request.form['joineddate']

        sql = "insert into employee_table(NAME,ADDRESS,GENDER,JOINEDDATE) values (%s,%s,%s,%s)"
        con.execute(sql,[name,address,gender,joineddate])
        db.connection.commit()
        con.close()
        flash('User Details Added')
        return redirect(url_for("Index"))
    return render_template("include.html")



@app.route("/edit/<string:id>", methods=['GET','POST'])
def edit(id):
    con = db.connection.cursor()
    if request.method == 'POST':
        name = request.form['name']
        address = request.form['address']
        gender = request.form['gender']
        joineddate = request.form['joineddate']
        sql = "update employee_table set NAME=%s,ADDRESS=%s,GENDER=%s,JOINEDDATE=%s where ID=%s"
        con.execute(sql, [name, address, gender, joineddate, id])
        db.connection.commit()
        con.close()
        flash('User Detail Updated')
        return redirect(url_for("Index"))
        con = mysql.connection.cursor()

    sql = "select * from employee_table where ID=%s"
    con.execute(sql, [id])
    res = con.fetchone()
    return render_template("updates.html", datas=res)


@app.route("/delete/<string:id>", methods=['GET', 'POST'])
def delete(id):
    con = db.connection.cursor()
    sql = "delete from employee_table where id=%s"
    con.execute(sql, id)
    db.connection.commit()
    con.close()
    flash('User Details Deleted')
    return redirect(url_for("Index"))
@app.route("/editsal/<string:id>", methods=['GET', 'POST'])
def editsal(id):
    con = db.connection.cursor()
    if request.method == 'POST':

        employeeid = request.form['employeeid']
        salary = request.form['salary']
        month = request.form['month']

        sql = "update salary set EMPID=%s,SALARY=%s,MONTH=%s where ID=%s"
        con.execute(sql,[employeeid,salary,month,id])
        db.connection.commit()
        con.close()
        flash('User Detail Updated')
        return redirect(url_for("sal"))
        con = mysql.connection.cursor()

    sql = "select * from salary where ID=%s"
    con.execute(sql, [id])
    res = con.fetchone()
    return render_template("editsal.html", datas=res)
@app.route("/deletesal/<string:id>", methods=['GET', 'POST'])
def deletesal(id):
    con = db.connection.cursor()
    sql = "delete from salary where id=%s"
    con.execute(sql, id)
    db.connection.commit()
    con.close()
    flash('User Details Deleted')
    return redirect(url_for("sal"))

@app.route("/sal")
def sal():
    con = db.connection.cursor()
    sql = "SELECT * FROM salary"
    con.execute(sql)
    salary = con.fetchall()
    return render_template("salary.html", value=salary)
@app.route("/addsalary/", methods=['GET', 'POST'])
def addsalary():
    if request.method == 'POST':
        employeeid = request.form['employeeid']
        salary = request.form['salary']
        month = request.form['month']
        con = db.connection.cursor()
        sql = "insert into salary(EMPID,SALARY,MONTH) value (%s,%s,%s)"
        con.execute(sql,[employeeid,salary,month])
        db.connection.commit()
        con.close()
        flash('salary Details Added')
        return redirect(url_for("sal"))
    return render_template("sal.html");





if (__name__ == '__main__'):
    app.secret_key = "abc123"
    app.run(debug=True)
